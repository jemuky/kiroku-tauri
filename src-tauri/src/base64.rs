use crate::ArrayRes;

use common::base64;

#[tauri::command]
pub fn encrypt_base64(text: String) -> ArrayRes {
    (0, base64::base64_encode(text.as_bytes()))
}

#[tauri::command]
pub fn decrypt_base64(text: String) -> ArrayRes {
    let de = base64::base64_decode(text.as_bytes());
    if de.is_err() {
        return (-1, de.unwrap_err().to_string());
    }
    (0, String::from_utf8_lossy(&de.unwrap()).to_string())
}
