#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

mod base64;
mod cripto;
mod img;

type ArrayRes = (i32, String);

use common::cm_log;
use tauri::State;

use std::sync::{Arc, Mutex};

fn main() {
    cm_log::log_init(common::LevelFilter::Debug);

    let context = tauri::generate_context!();
    tauri::Builder::default()
        .manage(Counter::default())
        .invoke_handler(tauri::generate_handler![
            hello_test,
            counter_add,
            img::img_merge,
            base64::encrypt_base64,
            base64::decrypt_base64,
            cripto::gen_pwd
        ])
        .menu(tauri::Menu::os_default(&context.package_info().name))
        .run(context)
        .expect("error while running tauri application");
}

#[tauri::command]
fn hello_test() -> String {
    String::from("Hello World!")
}

#[derive(Default)]
struct Counter(Arc<Mutex<i32>>);

#[tauri::command]
fn counter_add(n: i32, counter: State<'_, Counter>) -> String {
    let mut cnt = counter.0.lock().unwrap();
    *cnt += n;
    format!("{cnt}")
}
