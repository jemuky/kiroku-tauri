use crate::ArrayRes;

#[tauri::command]
pub fn gen_pwd(text: String) -> ArrayRes {
    (0, common::cripto::encrypt(&text))
}
