function chatgpt_open () {
    open_window("chatgpt", `
    <div style="width: 100%;height: 100%;display: flex;flex-direction: column;">
        <div style="width: 99%;height: 70%;border-style: inset;border: 1px dashed green;overflow-y: auto;">
            <div id="chat_div"></div>
            <div id="msg_end" style="height:0px; overflow:hidden"></div>
        </div>
        <div style="width: 100%;height: 30%;display: flex;">
            <textarea id="chat_input" cols="30" rows="10" style="width: 100%;"></textarea>
        </div>
    </div>
    `, 'html', '800px', '600px', false);
}

const open_ai_key = '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';

async function chat_send () {
    const content = document.getElementById('chat_input').value.trim();
    if (content == "") {
        return;
    }
    console.log(`content=${content}`);
    // 在公共聊天区域显示我的消息
    let chat_div = document.getElementById('chat_div');
    chat_div.innerText += `我 ${now_str()}\n`
    chat_div.innerText += `${content}\n\n`

    // 访问chatgpt
    let res = await request('https://api.openai.com/v1/chat/completions', {
        "model": "text-davinci-003",
        "messages": [{ "role": "user", "content": `${content}` }]
    }, 'post', {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${open_ai_key}`
    });
    console.log('res=', res);
    if (res.status != 200) {
        show(res.data.error.message, 1);
        return;
    }
    // 在公共聊天区域显示chatgpt的消息
    chat_div.innerText += `chatgpt ${now_str()}\n`
    chat_div.innerText += `${res.choices[0].message.content}\n\n`

    // 每次显示完自带滚动到最下面
    document.getElementById('msg_end').scrollIntoView();
    // 清空输入框
    document.getElementById('chat_input').value = '';
}

document.onkeyup = function (e) {
    if (e.key != 'Enter' && e.key != 'NumpadEnter') {
        return;
    }
    chat_send();
}