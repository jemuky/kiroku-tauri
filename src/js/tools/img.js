

function to_exit () {
    history.back();
}

function img_merge_open () {
    open_window('图片合并', `
    <div style="display: flex;
        flex-direction: column;
        justify-content: center;
        height: 100%;
        align-items: center;">

        <div style="display: flex;
            flex-direction: row;
            justify-content: center;
            width: 100%;
            align-items: center;">

            <div id="frontimg" style="display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
                width: 100%;">
                <button onclick="upload_frontground()" style="cursor: pointer;">前景图(背景须为透明，如png)</button>
                <img src="" alt="前景图" style="height: 100px;cursor: pointer;" class="btn-margin" id="img_merge_front" data-xphoto="img_merge_front" onclick="preview_img('img_merge_front')">
            </div>
            <div id="backimg" style="display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
                width: 100%;">
                <button onclick="upload_background()" style="cursor: pointer;">背景图</button>
                <img src="" alt="背景图" style="height: 100px;cursor: pointer;" class="btn-margin" id="img_merge_back" data-xphoto="img_merge_back" onclick="preview_img('img_merge_back')">
            </div>

        </div>

        <div class="btn-margin" id="result_img">
            <img src="" alt="处理结果" style="height: 100px;cursor: pointer;" id="img_merge_res" data-xphoto="img_merge_res" onclick="preview_img('img_merge_res')">
        </div>

        <div class="btn-margin">
            <button onclick="deal_img_merge()" style="cursor: pointer;">开始处理</button>
            <button onclick="save_deal_img()" style="cursor: pointer;">将处理结果保存到</button>
        </div>
    </div>
    `);
}

// data-xphoto的值和content一致
function preview_img (content) {
    xtip.open({
        type: 'photo',
        width: '60%',
        content: content,
        over: false,
        lock: true,
        min: true,
        max: true,
    });
}

async function save_deal_img () {
    const path = await dialog.save({
        multiple: false,
        filters: [{
            name: '',
            extensions: ['png']
        }]
    });

    if (!path) {
        toast("未选择路径", 1);
        return;
    }

    let img = local_path(document.getElementById('img_merge_res').src);
    if (!img) {
        toast('请先处理图片', 1);
        return;
    }

    let cp_res = await fs.copyFile(img, path);
    console.debug('cp_res=', cp_res);

    // 移除原路径的文件
    let rm_res = await fs.removeFile(img);
    console.debug('rm_res=', rm_res);

    toast('保存完成');
}

async function deal_img_merge () {
    const front_src = local_path(document.getElementById('img_merge_front').src);
    const back_src = local_path(document.getElementById('img_merge_back').src);
    if (!front_src || !back_src) {
        return;
    }
    console.debug('front_src:', front_src, ', back_src:', back_src)

    let index = loading();
    const res = await invoke('img_merge', { front: front_src, back: back_src });
    close_loading(index);

    if (res[0] != 0) {
        toast(res[1], 1);
        return;
    }
    document.getElementById('img_merge_res').src = convert_local_path(res[1]);
    toast('处理完成', 0);
}

async function upload_frontground () {
    const selected = await dialog.open({
        multiple: false,
    });

    if (!selected) {
        return;
    }
    document.getElementById('img_merge_front').src = convert_local_path(selected);
}

async function upload_background () {
    const selected = await dialog.open({
        multiple: false,
    });

    if (!selected) {
        return;
    }
    document.getElementById('img_merge_back').src = convert_local_path(selected);
}