
function pay_open () {
    open_window('pay', `
    <div style="display: flex;
        flex-direction: column;
        justify-content: center;
        height: 100%;
        align-items: center;">

        <div class="btn-margin">
            <button onclick="rsa_sign()" style="cursor: pointer;margin-bottom: 10px;">支付宝</button>
            <button onclick="create_order()" style="cursor: pointer;margin-bottom: 10px;">微信</button>
        </div>
    </div>
    `);
}
/**
 * 获取密钥
 */
async function get_secret_key () {
    let res = await request('/get_secret_key');
    console.debug(res);
}

const APP_ID = '2021000117638417';
const ALIPAY_PUB_KEY = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgMaJXppz1gnDoVyb2FtpYfHTORTtjvqzpQTpP2WTbRGQke3Eh6JKGizx+JX7Gd5IwrY2irO8mm4qC5oqi1n3zOcYOGwVE/v9cS3SFTWHbxjCCt6PjHvpaKd8yh6Om+frKrJ0OBdQYgUilf6tw6Htq8PXd3l7eQaVCgKKBAiDmmJ3qhf4cULf2ZDUHYmQyjUTGUaI5mhMyhfi8YooFAmETqr058nd7qzCZuvrrpl74TNdTs59x+7um1qCok12mQ9Hg8t5ZFjaARHqwJ0U4srJnr7kYaurKK/dHiOwhypMuknx9flJeib4ir7ebv+ohUSxRVsXr7202Mh+Nwyq7cZjdQIDAQAB';
const PUB_KEY = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2cP/VxN9t14MG1DGYh3gypK97KZxOsaBZt/Osb4bHCGglvQyhGeY3Whyj1KrynOQwgbZTXQWSBl9lGABJFVHji6hCFQUbljxGP98fFfZfUbMLVlHHpS0tOLfqMKaVMTp/nW7Vo3Ht4GXB3gVs70W9XG74gHw7Cc+IdcXQoWeAJbDn/EfEM/BAVZIBVuLeUV+1eBxnoVunMDQM/iBDy/KAPNggwgBlBVCtTGL64u5xLv21855QJ1l9xcx7i7ovFnX/bSsLt15HB3Rr52Z9zY203BEpxbUgYUNUjqRrNlamEx0LIzuA6YKozKvyyya8IvbSc8k10zTB7ej305F/2kjnwIDAQAB';
const PRIVATE_KEY = 'MIIEowIBAAKCAQEA2cP/VxN9t14MG1DGYh3gypK97KZxOsaBZt/Osb4bHCGglvQyhGeY3Whyj1KrynOQwgbZTXQWSBl9lGABJFVHji6hCFQUbljxGP98fFfZfUbMLVlHHpS0tOLfqMKaVMTp/nW7Vo3Ht4GXB3gVs70W9XG74gHw7Cc+IdcXQoWeAJbDn/EfEM/BAVZIBVuLeUV+1eBxnoVunMDQM/iBDy/KAPNggwgBlBVCtTGL64u5xLv21855QJ1l9xcx7i7ovFnX/bSsLt15HB3Rr52Z9zY203BEpxbUgYUNUjqRrNlamEx0LIzuA6YKozKvyyya8IvbSc8k10zTB7ej305F/2kjnwIDAQABAoIBAHjnKsMLnUbLPmmEBUDAWefeZPgYyNDbGUkmhTmF1MNfoCtvVQTSB7sVnFUajyqN0xcGemtYQm6xSFmym6+ycVHtbZaUJ+vEEcfRQSpAFA5L2lxrLuM5qMKtG4Q01z6tPUT71JEPkoy9tD5/W305RJ/tD2VBsqNKfmKHavCSAtFL3N81g2rzhLpm2TNGesxc2AYYf3Jh8NtsOwgZK7KWT6reU9gxshcNL2Ze6tCMhvuqR1wfV+0iaoXmxfdS+Md8F8y1eUrvgNNsq9KQuhETqTaSvgXx3Xm88Ptaz6UaUoj1C6MACECVl36/lo6LQlH74cKqKtv9XeGR+vYSEm402wECgYEA9SyrZyMXK7tAY3Y2CRf7f/3Z+HNAbYqIXOWSWdOuEX2koc8La3QMSzBsDyyzMYRo4m5MZRp3v0RAEM/HNT5nQZXG7pFkdR2xo4GpqPt+LZXMeQQL2ekO6KJ2Qigcc4+zOfprPUZxY/C0rghsztqXvgO3qYlR2aCKwcGUS9dWgUECgYEA42GC93Ub4whX6xn6Bz/04AyT/ffbbqYzghfJZl9N3bWSCwzvVEexdDjXGKVMAUcOSrJSiAJVRWEBoDJIAwGFyvwls9GPKmNU7P4JPaxqKclBi40Xpa2CYVwy1YGoELtn9ZkCcXS2EJTrsqoukyiN8bDkzM4Dkp6MC0ZCPZWfjN8CgYEAyMeqHq5n/VI9qrZWDiBrwCd9Z6/KpWp7vcfrkFCADqbcOf66lWbV4oVnGQ48OWL8GeLGAU4pnqMf7CScOQg/u0ATR7AdmqOUpBg9frtmW6J/buNXaT8pDG5vdkIu+pxF5Eclp2qW1Yd8D8GqicQN3Hhi4h4ov5Zvy7jh3zIcD4ECgYBZuKx5AJjUaQJoHoPuQGpbzhrHZ/oKG0LP3pr17j1HctIB0kTNuMxo09TsQK1RA7y79MlkKaeEDNcbYzvPE9a7Vet6nQIk87UbHtJ+p33spyFZ836VPXXo/PyCCvtKPuiynIG+SxNcT6cBhO2D28jGyco+ljwKqs0P1dVlf5jJeQKBgGFsyJ14V4VvpiRgJuyiwKKCtT02sUHCb2NmKoMjAZlFA8uUNhSWW4QESiWwFZ+ICeSg/GUD9ReXdZd7qjM2JcijmLtYLSI+Z8kUrmo1S+Dhfa+qDKJRgwpeD+q/jjBK7mbMr4u1lTQVcolEge0C/h2872nkHqz6CA21QsfYw/GZ';
/**
 * 创建订单
 */
async function create_order () {
    let res = await http.fetch(
        `https://openapi.alipaydev.com/gateway.do`,
        {
            method: 'get',
            query: {
                timestamp: now_str(),
                method: 'alipay.trade.create',
                app_id: APP_ID,
                sign_type: 'RSA2',
                sign: 'ERITJKEIJKJHKKKKKKKHJEREEEEEEEEEEE',
                version: '1.0',
                charset: 'UTF-8',
                biz_content: 'AlipayTradeCreateModel',
            }
        }
    );
    console.debug('res=', res);
}

async function rsa_sign () {
    let res = await http.fetch(
        'https://openapi.alipaydev.com/gateway.do',
        {
            query: {
                timestamp: now_str(),
                method: 'alipay.trade.create',
                version: '1.0',
                app_id: APP_ID,
                sign_type: 'RSA2',
                charset: 'UTF-8',
                biz_content: 'AlipayTradeCreateModel',
                // app_cert_sn: '123',
                // alipay_root_cert_sn: '456'
            }
        }
    );
    console.debug('res=', res);
}