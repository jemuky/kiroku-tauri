function gen_pwd_open () {
    open_window('生成密码', `
    <div style="display: flex;
        flex-direction: column;
        justify-content: center;
        height: 100%;
        align-items: center;">

        <input type="text" name="" id="gen_pwd_cipertext" style="width: 90%;" placeholder="请输入密码，格式：前后不能有空格，字符数量大于0">

        <textarea id="cipertext_after_encrypt" style="width: 90%;height: 90%;" placeholder="密文" class="btn-margin" disabled></textarea>

        <button class="btn-margin" onclick="gen_pwd()" style="cursor: pointer;margin-bottom: 10px;">生成</button>
    </div>
    `);
}

async function gen_pwd () {
    let pwd = document.getElementById('gen_pwd_cipertext').value.trim();
    if (!pwd) {
        toast('密码格式有误', 1);
        return;
    }

    let res = await invoke('gen_pwd', { text: pwd });
    deal_array_res(res, 'cipertext_after_encrypt');
}