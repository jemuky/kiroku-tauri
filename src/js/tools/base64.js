function base64_open () {
    open_window('base64', `
    <div style="display: flex;
        flex-direction: column;
        justify-content: center;
        height: 100%;
        align-items: center;">

        <div style="display: flex;
            flex-direction: row;
            justify-content: center;
            width: 100%;
            height: 100%;
            align-items: center;">

            <textarea id="origin_text" style="width: 90%;height: 90%;margin: 5px;" placeholder="原文"></textarea>
            <textarea id="res_text" style="width: 90%;height: 90%;margin: 5px;" placeholder="密文"></textarea>

        </div>

        <div class="btn-margin">
            <button onclick="encrypt_base64()" style="cursor: pointer;margin-bottom: 10px;">加密</button>
            <button onclick="decrypt_base64()" style="cursor: pointer;margin-bottom: 10px;">解密</button>
        </div>
    </div>
    `);
}

async function encrypt_base64 () {
    let res = await invoke('encrypt_base64', { text: document.getElementById('origin_text').value })
    deal_array_res(res, 'res_text');
}

async function decrypt_base64 () {
    let res = await invoke('decrypt_base64', { text: document.getElementById('res_text').value })
    deal_array_res(res, 'origin_text');
}