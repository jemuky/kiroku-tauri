
#### 弹窗依赖
https://github.com/ovsexia/xtiper

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Tauri](https://marketplace.visualstudio.com/items?itemName=tauri-apps.tauri-vscode) + [rust-analyzer](https://marketplace.visualstudio.com/items?itemName=rust-lang.rust-analyzer)

# 前置条件
系统有webview2

# kiroku-tauri

#### 介绍
kiroku前端，tauri版

tauri官网: https://tauri.app  

#### 预准备 
安装tauri-cli: cargo install tauri-cli  

#### 创建
cargo install create-tauri-app  
cargo create-tauri-app  

#### info
cargo tauri info  

#### 运行  
cargo tauri dev  

#### 打包
cargo tauri build

#### 版本
cargo tauri --version
